package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class TypesDetail {
    @SerializedName("slot") private String slot;
    @SerializedName("type") private Object type;

    public String getSlot() {
        return slot;
    }

    public Object getType() {
        return type;
    }
}

