package com.example.tanika.digiopokedex;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import com.example.tanika.digiopokedex.models.Pokemon;
import com.example.tanika.digiopokedex.models.PokemonRequest;
import com.example.tanika.digiopokedex.pokeapi.PokemonapiService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends AppCompatActivity {

    private Retrofit retrofit;

    private RecyclerView recyclerView;
    private PokemonAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<Pokemon> list;
    private boolean isScrolling = false;
    private int currentItems, totalItems,scrollOutItems;
    private ProgressBar progressBar;
    int offset = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        pokemonData();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new PokemonAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(Main2Activity.this);
        recyclerView.setLayoutManager(layoutManager);
        //layoutManager = new LinearLayoutManager(this);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems)){

                    isScrolling = false;
                    fetchData();
                }
            }
        });
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuRefresh :
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchData() {
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0 ;i<1;i++){

                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    i = 0;
                    loadmore();
                }
            }
        },5000);
    }

    private void loadmore() {
        PokemonapiService service = retrofit.create(PokemonapiService.class);
        Call<PokemonRequest> requestCall = service.nextPage(offset, 20);
        requestCall.enqueue(new Callback<PokemonRequest>() {
            @Override
            public void onResponse(Call<PokemonRequest> call, Response<PokemonRequest> response) {
                if (response.isSuccessful()){

                    PokemonRequest pokemonRequest = response.body();
                    list = pokemonRequest.getResults();

                    adapter.addListPokemon(list);
                    offset += 20 ;
                }else {
                    Log.d("POKE","onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonRequest> call, Throwable t) {

            }
        });
    }


    private void pokemonData() {
        PokemonapiService service = retrofit.create(PokemonapiService.class);
        Call<PokemonRequest> requestCall = service.nextPage(offset,20);

        requestCall.enqueue(new Callback<PokemonRequest>() {
            @Override
            public void onResponse(Call<PokemonRequest> call, Response<PokemonRequest> response) {
                if (response.isSuccessful()){

                    PokemonRequest pokemonRequest = response.body();
                    list = pokemonRequest.getResults();

                    adapter.addListPokemon(list);

                    Object obj = pokemonRequest.getNext();
                    Gson gson = new Gson();
                    String json = gson.toJson(obj);
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    offset += 20;
                    Log.d("testtest",json);
                }else {
                    Log.d("POKE","onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonRequest> call, Throwable t) {

                Log.d("POKE","onFailure: " + t.getMessage());

            }
        });
    }
}
