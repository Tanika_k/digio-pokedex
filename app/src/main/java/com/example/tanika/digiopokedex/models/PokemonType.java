package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonType {
    @SerializedName("types") private List<TypesDetail> list ;

    public List<TypesDetail> getList() {
        return list;
    }

}
