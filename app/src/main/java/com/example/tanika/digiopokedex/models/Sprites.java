package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Sprites {
    @SerializedName("sprites") private Object sprites;
    @SerializedName("forms") private ArrayList<Pokemon> forms;
    @SerializedName("id") private int pokemonId ;

    public Object getSprites() {
        return sprites;
    }

    public ArrayList<Pokemon> getForms() {
        return forms;
    }

    public int getPokemonId() {
        return pokemonId;
    }
}
