package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

public class Stat {

    @SerializedName("base_stat") private String base_stat;
    @SerializedName("stat") private Object stat;

    public String getBase_stat() {
        return base_stat;
    }

    public Object getStat() {
        return stat;
    }
}
