package com.example.tanika.digiopokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.tanika.digiopokedex.models.Pokemon;
import com.example.tanika.digiopokedex.models.PokemonRequest;
import com.example.tanika.digiopokedex.models.PokemonType;
import com.example.tanika.digiopokedex.models.Sprites;
import com.example.tanika.digiopokedex.models.Stat;
import com.example.tanika.digiopokedex.models.Stats;
import com.example.tanika.digiopokedex.models.TypesDetail;
import com.example.tanika.digiopokedex.pokeapi.PokemonapiService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main3Activity extends AppCompatActivity {

    public String  pokemonName, text = "";
    public String indexImg, img1,img2,img3,img4;
    public String pokemonTypes = "";
    private TextView pokemonNameTv,pokeTypeTv, tvAtk, tvDef ,tvSpeed ,tvSpAtk, tvSpDef;
    private ImageView pokemonImg1, pokemonImg2, pokemonImg3, pokemonImg4;
    private Retrofit retrofit;
    private PokemonapiService service;
    private ImageButton next, prev;
    private int pokemonId;
    private ArrayList<Pokemon> forms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Intent intent = getIntent();
        indexImg = intent.getStringExtra("index");
        pokemonName = intent.getStringExtra("pokemonName");


        pokemonNameTv = (TextView)findViewById(R.id.pokemonNameMain3);
        pokemonNameTv.setText(pokemonName);

        pokemonImg1 = (ImageView)findViewById(R.id.poke1);
        pokemonImg2 = (ImageView)findViewById(R.id.poke2);
        pokemonImg3 = (ImageView)findViewById(R.id.poke3);
        pokemonImg4 = (ImageView)findViewById(R.id.poke4);

        pokeTypeTv = (TextView)findViewById(R.id.tvType);
        tvAtk = (TextView)findViewById(R.id.tvAtk);
        tvDef = (TextView)findViewById(R.id.tvDef);
        tvSpeed = (TextView)findViewById(R.id.tvSpeed);
        tvSpAtk = (TextView)findViewById(R.id.spAtkTv);
        tvSpDef = (TextView)findViewById(R.id.spDefTv);

        next = (ImageButton) findViewById(R.id.nextBtn);
        prev = (ImageButton)findViewById(R.id.prevBtn);

        if(pokemonName.equalsIgnoreCase("bulbasaur")){
            prev.setVisibility(View.GONE);
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        try {
            Log.d("pokePosition3", String.valueOf(indexImg));
        }catch (Exception e){

        }

        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        getPokemonType();
        getPokemonImg();
        getPokemonStatus();
    }

    private void nextPokemon() {
        PokemonapiService service = retrofit.create(PokemonapiService.class);
        Call<Sprites> requestCall = service.nextPokemon(pokemonId,1);

        requestCall.enqueue(new Callback<Sprites>() {
            @Override
            public void onResponse(Call<Sprites> call, Response<Sprites> response) {
                if (response.isSuccessful()){

                    Sprites sprites = response.body();
                }else {
                    Log.d("POKE","onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Sprites> call, Throwable t) {

            }


        });
    }

    private void getPokemonStatus() {
        PokemonapiService service = retrofit.create(PokemonapiService.class);
        Call<Stats> statsCall = service.getPokemonStat(pokemonName);
        statsCall.enqueue(new Callback<Stats>() {
            @Override
            public void onResponse(Call<Stats> call, Response<Stats> response) {
                Stats stats = response.body();
                ArrayList<Stat> statArrayList = stats.getStats();
                for(int i = 0 ; i < statArrayList.size() -1 ; i++) {


                    Object list = statArrayList.get(i).getStat();
                    String s = statArrayList.get(i).getBase_stat();

                    try {
                        Gson gson = new Gson();
                        String json = gson.toJson(list);
                        JSONObject jsonObject = new JSONObject(json);

                        String pokemonStat = jsonObject.getString("name").toString() + " , Base = " + s;
                        Log.d("POKEomgomg",pokemonStat);

                        switch(i) {
                            case 0:
                                tvSpeed.setText(s);
                                break;
                            case 1:
                                tvSpDef.setText(s);
                                break;
                            case 2:
                                tvSpAtk.setText(s);
                                break;
                            case 3:
                                tvDef.setText(s);
                                break;
                            case 4:
                               tvAtk.setText(s);
                                break;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                }

            @Override
            public void onFailure(Call<Stats> call, Throwable t) {

            }
        });
    }

    private void getPokemonImg() {
        PokemonapiService service = retrofit.create(PokemonapiService.class);
        Call<Sprites> spritesCall = service.getPokemonDetail(pokemonName);
        spritesCall.enqueue(new Callback<Sprites>() {
            @Override
            public void onResponse(Call<Sprites> call, Response<Sprites> response) {
                Sprites sprites = response.body();

                pokemonId = sprites.getPokemonId();

                Object o = sprites.getSprites();
                try {
                    Gson gson = new Gson();
                    String json = gson.toJson(o);
                    JSONObject jsonObject = new JSONObject(json);

                    img1 = jsonObject.getString("front_default").toString();
                    img2 = jsonObject.getString("back_default").toString();
                    img3 = jsonObject.getString("front_shiny").toString();
                    img4 = jsonObject.getString("back_shiny").toString();

                    Log.d("POKEomgomg","onResponse: " + img1);

                    showPokemonImage();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Sprites> call, Throwable t) {

            }
        });

    }

    private void getPokemonType() {

        PokemonapiService service = retrofit.create(PokemonapiService.class);

        Call<PokemonType> typeCall = service.getPokemonTypes(pokemonName);
        typeCall.enqueue(new Callback<PokemonType>() {
            @Override
            public void onResponse(Call<PokemonType> call, Response<PokemonType> response) {
                if (response.isSuccessful()){

                   PokemonType pokemonType = response.body();
                   List<TypesDetail> listPoke = pokemonType.getList();
                   //pokemonTypes = listPoke.size();

                   for(int i = 0 ; i < listPoke.size(); i++){
                       Object list = pokemonType.getList().get(i).getType();

                       try {
                           Gson gson = new Gson();
                           String json = gson.toJson(list);
                           JSONObject jsonObject = new JSONObject(json);

                           pokemonTypes += jsonObject.getString("name").toString() + " ,";

                       } catch (JSONException e) {
                           e.printStackTrace();
                       }

                       Log.d("POKEomgomg","onResponse: " + pokemonTypes + list.toString());
                       text += list.toString();
                   }
                   if (pokemonTypes.endsWith(",")) {
                        pokemonTypes = pokemonTypes.substring(0, pokemonTypes.length() - 1);
                        pokeTypeTv.setText(pokemonTypes);
                    }

                }else {
                    Log.d("POKEomgomg","onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonType> call, Throwable t) {
                Log.d("POKEomgomg","onFailure: " + t.getMessage());
            }
        });
    }

    private void showPokemonImage() {

        Glide.with(this)
                .load(img1)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(pokemonImg1);


        Glide.with(this)
                .load(img2)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(pokemonImg2);

        Glide.with(this)
                .load(img3)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(pokemonImg3);

        Glide.with(this)
                .load(img4)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(pokemonImg4);


    }

}
