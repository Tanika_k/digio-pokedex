package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PokemonRequest {

    private ArrayList<Pokemon> results;

    @SerializedName("next") private Object next;

    public Object getNext() {
        return next;
    }

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
