package com.example.tanika.digiopokedex;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.tanika.digiopokedex.models.Pokemon;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    private ArrayList<Pokemon> pokemonData;
    private Context context;


    public PokemonAdapter(Context context) {
        this.pokemonData = new ArrayList<>();
        this.context = context;
    }


    @NonNull
    @Override
    public PokemonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemonitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PokemonAdapter.ViewHolder holder, int position) {


        final Pokemon pokemon = pokemonData.get(position);
        holder.pokemonName.setText(pokemon.getName());
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+ pokemon.getImgOrder()+".png")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.pokemonImg);


        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {
                if (isLongClick){

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("ต้องการลบ " + pokemon.getName() + "?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    pokemonData.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position,pokemonData.size());
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }else {
                    Intent intent = new Intent (view.getContext(), Main3Activity.class);
                    intent.putExtra("index" ,String.valueOf(position+1));
                    intent.putExtra("pokemonName", pokemonData.get(position).getName());
                    Log.d("pokePosition", String.valueOf(position));
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return pokemonData.size();
    }

    public void addListPokemon(ArrayList<Pokemon> list) {
        pokemonData.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {

        private ImageView pokemonImg ;
        private TextView pokemonName;
        public ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);

            pokemonImg = itemView.findViewById(R.id.pokemonPic);
            pokemonName = itemView.findViewById(R.id.pokemonName);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);


        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true);
            return false;
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
    }


}
