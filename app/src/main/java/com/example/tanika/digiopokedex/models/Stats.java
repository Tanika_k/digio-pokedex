package com.example.tanika.digiopokedex.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Stats {
    @SerializedName("stats") private ArrayList<Stat> stats;

    public ArrayList<Stat> getStats() {
        return stats;
    }
}
