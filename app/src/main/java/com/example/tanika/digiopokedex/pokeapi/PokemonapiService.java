package com.example.tanika.digiopokedex.pokeapi;

import com.example.tanika.digiopokedex.models.PokemonRequest;
import com.example.tanika.digiopokedex.models.PokemonType;
import com.example.tanika.digiopokedex.models.Sprites;
import com.example.tanika.digiopokedex.models.Stats;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PokemonapiService {

    @GET("pokemon")
    Call<PokemonRequest> dataListPokemon();

    @GET("pokemon/{pokemonId}/")
    Call<PokemonType> getPokemonTypes(@Path("pokemonId") String pokemonId);

    @GET("pokemon/{pokemonId}/")
    Call<Sprites> getPokemonDetail(@Path("pokemonId") String pokemonId);

    @GET("pokemon/{pokemonId}/")
    Call<Stats> getPokemonStat(@Path("pokemonId") String pokemonId);

    @GET("pokemon?")
    Call<PokemonRequest> nextPage(@Query("offset") int ofset , @Query("limit") int limit);

    @GET("pokemon?")
    Call<Sprites> nextPokemon(@Query("offset") int ofset , @Query("limit") int limit);
}
