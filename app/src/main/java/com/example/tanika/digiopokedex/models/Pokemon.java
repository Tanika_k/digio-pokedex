package com.example.tanika.digiopokedex.models;

public class Pokemon {

    int imgOrder;
    private String name ;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getImgOrder() {
        String[] urlCut = url.split("/");
        return Integer.parseInt(urlCut[urlCut.length -1]);
    }

    public void setImgOrder(int imgOrder) {
        this.imgOrder = imgOrder;
    }


}
